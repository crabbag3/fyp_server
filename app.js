require("dotenv").config();
const express = require("express");
const app = express();
const sls = require('serverless-http')
const cors = require("cors");
const bodyParser = require("body-parser");
const errorHandler = require("./handlers/error");
const authRoutes = require("./routes/auth");
const tripsRoutes = require("./routes/trips");
const { loginRequired, ensureCorrectUser } = require("./middleware/auth");
const db = require("./models");
const PORT = process.env.PORT || 1337;


app.use(cors());
app.use(bodyParser.json());

app.use("/api/auth", authRoutes);
app.use(
  "/api/users/:id/trips",
  loginRequired,
  ensureCorrectUser,
  tripsRoutes
);


// Fetch all trips
app.get("/api/trips", loginRequired, async function(req, res, next) {
  try {
    let trips = await db.Trip.find()
      .sort({ createdAt: "asc" })
      .populate("user", {
        name: true,        
      });
    return res.status(200).json(trips);
  } catch (err) {
    return next(err);
  }
});


// fetch user list (don't fetch sensitive data) 
app.get("/api/users", loginRequired, async function(req, res, next) {
  // finding a user
  try {
		let user = await db.User.find({
			completedTripCount: {$gt: 1}},
			 {password:0, trips:0, email:0 })
			.sort({ points: "desc"})
			.sort({ completedTripCount: "desc"})

    let { name, points, completedTripCount, } = user;
      return res.status(200).json({user
      });

  } catch (e) {
    return next(err);
  }
});

app.use(function(req, res, next) {
  let err = new Error("Not Found");
  err.status = 404;
  next(err);
});

app.use(errorHandler);

app.listen(PORT, function() {
  console.log('Server is starting on port: ' + PORT);
});

module.exports.server = sls(app)