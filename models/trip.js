const mongoose = require("mongoose");
const User = require("./user");

const tripSchema = new mongoose.Schema(

  {
    text: {
      type: String,
      required: false,
      maxLength: 36
		},
		owner: 
			{
			name: {
				type: String,
				required: true
			},
			id: {
				type: String,
				required: true,
			},
			completed: {
				type: Boolean,
				required: true,
			}
		},
	
		location_from: {
			type: String,
			required: true,
			maxLength: 16
		},
		location_from_id: {
			type: String,
			required: true
		},
		location_to: {
			type: String,
			required: true,
			maxLength: 16
		},
			/*
				main_text: {
				type: String,
				required: true,
				maxLength: 16
			},
			sub_text: {
				type: String,
				required: true,
				maxLength: 32
			},
			id: {
				type: String,
				required: true
			},
			coords: {
				latitude: {
					type: Number,
					required: true
				},
				longitude: {
					longitude: Number,
					required: true
				}
					}
			
	}, */
		location_to_id: {
			type: String,
			required: true
		},
		dateISO: {
			type: Date,
			required: true,
		},
		timeStamp: {
			type: String,
			required: true
		},
		spaces: {
			type: Number,
			required: true,
		},
		active: {
			type: Boolean,
			required: true
		},
		completed: {
			type: Boolean,
			required: true
			
		},
		distanceTravelled: {
			type: Number,
		},
		user_confirm: 
			{
			id: {
				type: String,
				required: true,
				},
			completed: {
				type: Boolean,
				required: true
			}
		},
	
		tripScore : {
			type: String,
	
		},
		
		user: [
			{
				type: mongoose.Schema.Types.ObjectId,
				ref: "User",
			},
		]
	},
  {
    timestamps: true
  }
);

tripSchema.pre("remove", async function(next) {
  try {
    // find a user
    let user = await User.findById(this.user);
    // remove the id of the trip from their trips list
    user.trips.remove(this.id);
    // save that user
    await user.save();
    // return next
    return next();
  } catch (err) {
    return next(err);
  }
});

const Trip = mongoose.model("Trip", tripSchema);
module.exports = Trip;
